def modify_list(l):
	c = 0
	while c < len(l):
		if l[c] % 2 == 0:
			l[c] //= 2
			c += 1
		else:
			del l[c]
lst = [1, 2, 3, 4, 5, 6]
print(modify_list(lst))  # None
print(lst)               # [1, 2, 3]
modify_list(lst)
print(lst)               # [1]

lst = [10, 5, 8, 3]
modify_list(lst)
print(lst)               # [5, 4]